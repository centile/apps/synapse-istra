# Copyright 2017 New Vector Ltd.
import json
import logging
import os
from io import BytesIO

import yaml
from synapse.logging.context import PreserveLoggingContext
from synapse.util.logcontext import make_deferred_yieldable
from twisted.internet import defer, reactor
from twisted.web.client import (
    Agent, FileBodyProducer, readBody,
)
from twisted.web.http_headers import Headers

logger = logging.getLogger(__name__)


class HttpResponseException(Exception):
    """
    Represents an HTTP-level failure of an outbound request

    Attributes:
        code (int): HTTP error code
        msg (str): string describing the error
        response (str): body of response
    """

    def __init__(self, code, msg, response):
        """
        Args:
            code (int): HTTP status code
            msg (str): reason phrase from HTTP response status line
            response (str): body of response
        """
        super(HttpResponseException, self).__init__("%d: %s" % (code, msg))
        self.code = code
        self.msg = msg
        self.response = response


class IstraClient(object):
    """Interface to the Istra REST API
    """

    dict_istra = {}

    def __init__(self, istra_server, config):
        # add a trailing '/' here if necessary so we don't have to check every
        # time

        # if config is None exception

        print(("################# IstraClient #" + str(istra_server) + "  @ " + str(config)))
        if not config.is_multi_istra:
            if istra_server[-1] != '/':
                istra_server += '/'
        else:
            istra_server = None
        self._istra_server = istra_server
        self._config = config
        self._request_timeout_ms = 10000
        self._agent = Agent(
            reactor,
            connectTimeout=15,
        )

    def get_uri(self, user):
        """
        """
        tuser = user.split("_")
        if len(tuser) == 1 or user == "" or user[0] != '@':
            return None        
        #filename = tuser[0][1:] + ".yaml"
        clusterName = tuser[0][1:].split(".")[0]
        filename = clusterName + ".yaml"
        print(("################# get_uri(" + str(user) + ") => filename=" + str(filename)))

        if filename in list(IstraClient.dict_istra.keys()):
            return IstraClient.dict_istra[filename]
        else:
            with open(os.path.join(self._config.istra_multi_filename, filename)) as stream:
                try:
                    data = yaml.load(stream, Loader=yaml.FullLoader)
                    url = data['url']
                    if url[-1] != '/':
                        url += '/'
                    IstraClient.dict_istra[filename] = url
                    return IstraClient.dict_istra[filename]
                except yaml.YAMLError as exc:
                    print((str(exc)))
                    return None

    @defer.inlineCallbacks
    def check_login(self, login_type, user, password):
        """Notify the istra server of a login attempt

        Args:
            login_type (str): type of login (com.centile.login.istra, probably)
            user (str): qualified @user:id
            password (str): login token provided by client

        Returns:
            defer.Deferred[str]: client token from istra server

        Raises:
            HTTPResponseException if the istra server returns a non-200
        """
        if not self._config.is_multi_istra:
            server_uri = self._istra_server + "users/login"
        else:
            server_uri = self.get_uri(user) + "users/login"

        logger.debug("check_login for %s @ %s", user, server_uri)
        response = yield self._post_json_get_json(
            str.encode(server_uri),
            {
                "type": login_type,
                "user": user,
                "password": password,
            }
        )
        logger.debug("check_login => %s", response)
        client_token = response.get("client_token")
        if not client_token:
            raise Exception(
                "Invalid response from istra server: no client_token"
            )
        defer.returnValue(client_token)

    @defer.inlineCallbacks
    def logout(self, user, client_token):
        """Notify the istra server of a client logout

        Args:
            user (str): qualified @user:id
            client_token (str): istra server token for this session

        Returns:
            defer.Deferred
        """
        if not self._config.is_multi_istra:
            server_uri = self._istra_server + "users/logout"
        else:
            server_uri = self.get_uri(user) + "users/logout"

        yield self._post_json_get_json(
            str.encode(server_uri),
            {
                "user": user,
                "client_token": client_token,
            }
        )

    @defer.inlineCallbacks
    def _request(self, method, uri, *args, **kwargs):
        """Wrapper for agent.Request

        handles logcontexts and timeouts

        Returns:
            Deferred[Response]
        """

        def send_request():
            logger.debug("Sending request : %s %s (args=%s / kwargs=%s)", method, uri, str(args), str(kwargs))

            request_deferred = self._agent.request(
                method, uri, *args, **kwargs
            )
            request_deferred.addTimeout(
                int(self._request_timeout_ms / 1000),
                reactor,
            )

            return request_deferred

        try:
            with PreserveLoggingContext():
                response = yield send_request()

            logger.info(
                "Received response to  %s %s: %s",
                method, uri, response.code
            )

            defer.returnValue(response)

        except Exception as e:
            logger.info(
                "Error sending request to %s %s: %s %s",
                method, uri, type(e).__name__, e.message
            )
            raise

    @defer.inlineCallbacks
    def _post_json_get_json(self, uri, post_json):
        """

        Args:
            uri (str):
            post_json (object):

        Returns:
            Deferred[object]: parsed json

        Raises:
            HTTPResponseException if the istra server returns a non-200
        """
        json_str = json.dumps(post_json)
        logger.debug("HTTP POST %s -> %s", json_str, uri)

        response = yield self._request(
            b"POST",
            uri,
            Headers({"Content-Type": ["application/json"]}),
            FileBodyProducer(BytesIO(str.encode(json_str)))
        )

        body = yield make_deferred_yieldable(readBody(response))

        if 200 <= response.code < 300:
            defer.returnValue(json.loads(body))
        else:
            raise HttpResponseException(
                response.code, response.phrase, body
            )
