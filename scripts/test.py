#!/usr/bin/env python
#
# Quick test script to see if the istra integration is working



import logging
import random
import string
import threading
import urllib.request, urllib.parse, urllib.error

import requests

from test_server import istra_tokens, build_httpd

SYNAPSE_SERVER = 'http://localhost:8008'
ISTRA_AS_TOKEN = 'Icuih1raphaikee9ip5poo0jeeyoogoo'

ISTRA_LOGIN_TYPE = "com.centile.login.istra"
PASSWORD_LOGIN_TYPE = "m.login.password"

logger = logging.getLogger()


def main():
    logging.basicConfig(level=logging.INFO)
    httpd = start_httpd()
    try:
        run_tests()
    finally:
        httpd.shutdown()


def start_httpd():
    httpd = build_httpd()
    th = threading.Thread(target=httpd.serve_forever)
    th.start()
    return httpd


# the localpart of our test user
user_name = None

# the mxid of our test user
user_id = None


def run_tests():
    global user_name, user_id

    # Start by registering a user
    user_name = '_test_user_' + ''.join(
        random.choice(string.ascii_lowercase) for _ in range(4)
    )
    print("user_name %s" % user_name)

    r = requests.post(
        SYNAPSE_SERVER+'/_matrix/client/r0/register',
        json={
            "inhibit_login": True,
            "username": user_name,
        },
        auth=appserv_auth,
    )
    r.raise_for_status()
    res = r.json()

    user_id = res["user_id"]
    print("registered new user %s" % (user_id, ))

    test_set_displayname()
    test_istra_login()
    test_password_login()
    test_rejected_password_login()
    test_invalidate()
    test_deactivate()


def test_set_displayname():
    print("---- Testing set displayname as istra server ----")
    uri = SYNAPSE_SERVER + (
        '/_matrix/client/r0/profile/%s/displayname' % (
            urllib.parse.quote(user_id),
        )
    )
    requests.put(
        uri, params={"user_id": user_id},
        json={"displayname": "test_user"},
        auth=appserv_auth,
    ).raise_for_status()


def test_istra_login():
    print("---- Testing istra login ----")
    res = log_in(user_name, ISTRA_LOGIN_TYPE)
    assert (res["user_id"] == user_id)
    log_out(res["access_token"])
    assert(len(istra_tokens) == 0)


def test_password_login():
    print("---- Testing password login ----")
    res = log_in(user_name, PASSWORD_LOGIN_TYPE)
    assert(len(istra_tokens) == 1)
    log_out(res["access_token"])
    assert(len(istra_tokens) == 0)


def test_rejected_password_login():
    print("---- Testing failing login ----")

    # if istra gives a 401, synapse should return a 403 from /login
    r = requests.post(
        SYNAPSE_SERVER + '/_matrix/client/r0/login',
        json={
            "type": PASSWORD_LOGIN_TYPE,
            "user": user_name,
            "password": "err_401",
        },
    )
    assert(r.status_code == 403)


def test_invalidate():
    print("---- Testing session invalidation from istra server ----")
    log_in(user_id, PASSWORD_LOGIN_TYPE)

    assert(len(istra_tokens) == 1)

    # and invalidate
    token = list(istra_tokens)[0]
    r = requests.post(
        SYNAPSE_SERVER + '/_matrix/invalidate_session',
        params={"user_id": user_id},
        json={"istra_token": token},
        auth=appserv_auth,
    )
    r.raise_for_status()


def test_deactivate():
    print("---- Testing account deactivation from istra server ----")

    # deactivate
    r = requests.post(
        SYNAPSE_SERVER+'/_matrix/client/r0/account/deactivate',
        params={"user_id": user_id},
        auth=appserv_auth,
        json={}
    )
    r.raise_for_status()

    # check can still log in after deactivate. It's up to the istra server
    # to prevent this, not us.
    res = log_in(user_id, ISTRA_LOGIN_TYPE)
    assert(res["user_id"] == user_id)


def log_in(user, login_type):
    # log in as the user
    r = requests.post(
        SYNAPSE_SERVER + '/_matrix/client/r0/login',
        json={
            "type": login_type,
            "user": user,
            "password": "abc",
        },
    )
    r.raise_for_status()
    res = r.json()
    print("logged in")
    return res


def log_out(access_token):
    # log out as the user
    r = requests.post(
        SYNAPSE_SERVER + '/_matrix/client/r0/logout',
        params={"access_token": access_token},
        json={},
    )
    r.raise_for_status()
    print("logged out")


def appserv_auth(req):
    req.headers['Authorization'] = 'Bearer ' + ISTRA_AS_TOKEN
    return req


if __name__ == "__main__":
    main()
