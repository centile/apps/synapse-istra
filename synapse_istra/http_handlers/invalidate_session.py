# Copyright 2017 Vector Creations Ltd.
import json
import logging

from twisted.internet import defer

from synapse_istra.storage.tokenstore import TokenStore

logger = logging.getLogger(__name__)


class InvalidateSessionHandler(object):
    def __init__(self, config, api):
        self._api = api
        self._token_store = TokenStore(api)

    @defer.inlineCallbacks
    def handle_request(self, request):
        # first, authenticate the request
        requester = yield self._api.get_user_by_req(request)

        if requester.app_service is None:
            respond_with_json(request, 403, {
                "error": "Invalid credentials",
                "errcode": "M_FORBIDDEN",
            })
            return

        jsonobj = json.load(request.content)
        if type(jsonobj) != dict:
            respond_with_json(request, 400, {
                "error": "Content must be a JSON object.",
                "errcode": "M_BAD_JSON",
            })

        istra_token = jsonobj.get("istra_token")
        if istra_token is None:
            respond_with_json(request, 400, {
                "error": "Invalid request",
                "errcode": "M_BAD_JSON",
            })
            return

        access_token = yield self._token_store.delete_token_map_by_istra_token(
            istra_token=istra_token,
        )

        if access_token is None:
            respond_with_json(request, 404, {
                "error": "Unknown istra client token",
                "errcode": "M_NOT_FOUND",
            })
            return

        # XXX we should probably check that the user_id in the access token
        # corresponds to the user_id in the request. On the other hand the
        # AS is privileged anyway so <shrug>

        yield self._api.invalidate_access_token(access_token)
        respond_with_json(request, 200, {})

    @staticmethod
    def parse_config(config):
        return None


def respond_with_json(request, code, json_object):
    """Sends encoded JSON in response to the given request.

    Args:
        request (twisted.web.http.Request): The http request to respond to.
        code (int): The HTTP response code.
        json_object (object): The object to be sent as the response body.
    """
    json_bytes = json.dumps(json_object)

    request.setResponseCode(code)
    request.setHeader(b"Content-Type", b"application/json")
    request.setHeader(b"Content-Length", b"%d" % (len(json_bytes),))
    request.write(json_bytes)
    try:
        request.finish()
    except RuntimeError as e:
        logger.info(
            "Connection disconnected before response was written: %s", e,
        )
