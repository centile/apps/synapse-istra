Istra server integration for synapse
====================================

This python package provides the Istra integration for a synapse server.

Installation
------------

To install this module, activate the virtualenv used by synapse, and then run
`./setup.py install` in this directory.

Configuration
-------------

There are three parts to the configuration.

First we need to configure Istra as a password provider. In the `homeserver.yaml`,
add the following at the top level:

    password_providers:
      - module: "synapse_istra.PasswordProvider"
        config:
          istra_server: "https://<istra server>"

Then we need to configure the Istra server as an application service, so that
it can make matrix API calls on behalf of its users. Add the following to the
top level of `homeserver.yaml`:

    app_service_config_files:
      - "istra_as.yaml"

And create `istra_as.yaml` as follows:

    id: istra
    hs_token: ""
    as_token: <auth token that istra will use to authenticate to synapse>
    sender_localpart: ""
    url: null
    namespaces:
      users:
        - exclusive: true
          regex: "@.*"

Finally, we need need to enable the custom REST endpoints used by the Istra
server. Add the following under one or more `listeners` in `homeserver.yaml`:

    additional_resources:
      "/_matrix/invalidate_session":
        module: synapse_istra.InvalidateSessionHandler
        config: {}
